package com.minecraftitems.currency.items;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;

import java.util.List;

public class ItemPaperNote extends ItemCurrency {

    public ItemPaperNote() {
        super();
        this.setUnlocalizedName("ItemPaperNote");
    }

    @Override
    public void addInformation(ItemStack p_77624_1_, EntityPlayer p_77624_2_, List p_77624_3_, boolean p_77624_4_) {
        p_77624_3_.add("Worth: $10");
    }
}
