package com.minecraftitems.currency.items;

import com.minecraftitems.currency.money.MoneyManager;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

import java.util.HashMap;

public class ItemCreditCard extends ItemCurrency {

    private static boolean active = true;
    private static double startingAmount = 100.0;

    private static HashMap<String, Double> user = new HashMap<String, Double>();

    public ItemCreditCard() {
        super();
        this.setUnlocalizedName("ItemCreditCard");
    }

    @Override
    public ItemStack onItemRightClick(ItemStack p_77659_1_, World p_77659_2_, EntityPlayer p_77659_3_) {
        return p_77659_1_;
    }

    public void storedCredit(Double amount) {

    }

    public void removeCredit(Double amount) {

    }

    public static void addCredit(Double amount, EntityPlayer player) {
        World world = player.worldObj;
        String name = player.getDisplayName();
        String getUser = MoneyManager.checkUser(name);

        if (!world.isRemote)
        user.put(getUser, amount);
    }

    public void addUser(String name) {
        user.put(name, startingAmount);
    }

    public void removeUser(String name) {
        user.remove(name);
    }

}
