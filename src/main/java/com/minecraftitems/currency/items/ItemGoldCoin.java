package com.minecraftitems.currency.items;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;

import java.util.List;

public class ItemGoldCoin extends ItemCurrency {

    public ItemGoldCoin() {
        super();
        this.setUnlocalizedName("ItemCoin_gold");

    }

    @Override
    public void addInformation(ItemStack stack, EntityPlayer player, List list, boolean bool) {
        list.add("Worth: $2");
    }
}
