package com.minecraftitems.currency.items;

import com.minecraftitems.currency.money.MoneyManager;
import com.minecraftitems.currency.util.Log;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

import java.util.List;

public class ItemDebug extends ItemCurrency {

    public ItemDebug() {
        super();
        this.setUnlocalizedName("ItemDebug");
    }

    @Override
    public ItemStack onItemRightClick(ItemStack stack, World world, EntityPlayer player) {
        ItemCreditCard.addCredit(999999.9, (player));
        Log.info("Added $999999.9 to " + player.getDisplayName());
        return stack;
    }

    @Override
    public void addInformation(ItemStack p_77624_1_, EntityPlayer p_77624_2_, List p_77624_3_, boolean p_77624_4_) {
        p_77624_3_.add("For debugging only");
    }

}
