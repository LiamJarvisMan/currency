package com.minecraftitems.currency;

import com.minecraftitems.currency.init.ItemInit;
import com.minecraftitems.currency.proxy.IProxy;
import com.minecraftitems.currency.reference.Reference;
import com.sun.tools.internal.xjc.reader.RawTypeSet;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.SidedProxy;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPostInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;

@Mod(modid = Reference.MOD_ID, name = Reference.MOD_NAME, dependencies = Reference.DEPENDENCIES, version = Reference.MOD_VERSION)
public class currency {

    @Mod.Instance
    public static currency instance;

//    @SidedProxy(clientSide = Reference.CLIENT_PROXY_CLASS, serverSide = Reference.SERVER_PROXY_CLASS)
//    public static IProxy proxy;

    @Mod.EventHandler
    public static void preInit(FMLPreInitializationEvent event) {

        ItemInit.init();

    }

    @Mod.EventHandler
    public static void init(FMLInitializationEvent event) {



    }

    @Mod.EventHandler
    public static void postInit(FMLPostInitializationEvent event) {



    }


}
