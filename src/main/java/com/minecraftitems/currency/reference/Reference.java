package com.minecraftitems.currency.reference;

public class Reference {

    public static final String MOD_ID = "currency";
    public static final String MOD_NAME = "Currency";
    public static final String MOD_VERSION = "1.7.10-1.0";
    public static final String CLIENT_PROXY_CLASS = "com.minecraftitems.currency.ClientProxy";
    public static final String SERVER_PROXY_CLASS = "com.minecraftitems.currency.ServerProxy";
    public static final String DEPENDENCIES = "";


}
