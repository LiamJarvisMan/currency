package com.minecraftitems.currency.money;

import com.minecraftitems.currency.util.Log;

import java.util.HashMap;

public class MoneyManager {

    public static HashMap<String, String> dataStorage = new HashMap<String, String>();

    public static void addUser(String name, String uuid) {
        dataStorage.put(name, uuid);
    }

    public static void removeUser(String name, String uuid) {
        dataStorage.remove(name, uuid);
    }

    public static String checkUser(String name) {
       String test = dataStorage.get(name);
        Log.info(test);
        return name;
    }

}
