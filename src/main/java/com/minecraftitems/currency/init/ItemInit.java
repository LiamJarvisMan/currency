package com.minecraftitems.currency.init;

import com.minecraftitems.currency.items.*;
import cpw.mods.fml.common.registry.GameRegistry;

public class ItemInit {

    public static final ItemGoldCoin GOLD_COIN = new ItemGoldCoin();
    public static final ItemIronCoin IRON_COIN = new ItemIronCoin();
    public static final ItemPaperNote PAPER_NOTE = new ItemPaperNote();
    public static final ItemDebug DEBUG = new ItemDebug();
    public static final ItemCreditCard CREDIT_CARD = new ItemCreditCard();


    public static void init() {

        GameRegistry.registerItem(GOLD_COIN, "GoldCoin");
        GameRegistry.registerItem(IRON_COIN, "IronCoin");
        GameRegistry.registerItem(PAPER_NOTE, "PaperNote");
        GameRegistry.registerItem(DEBUG, "debug");
        GameRegistry.registerItem(CREDIT_CARD, "Credit Card");

    }

}
